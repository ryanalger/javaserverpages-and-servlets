# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Assignment 2 Requirements:

*Goals:*

1. Install mySQL
2. Develop and Deploy a WebApp
3. Deploy a Servlet using @WebServlet

#### Assignment Links:

1. [http://localhost:9999/hello](http://localhost:9999/hello)
2. [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)
3. [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
4. [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
5. [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)

#### Assignment Screenshots:

*Screenshot of Index Page*:

![Index Page](img/index_page.png)

*Screenshot of Query Results*:

![Query Results](img/query_results.png)

