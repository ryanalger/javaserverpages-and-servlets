# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Assignment 5 Requirements:

1. Clone starter files from Bitbucket
2. Work with MVC Pattern
3. Prepared statements to prevent SQL injection
4. Use JSTL to prevent XSS

#### Assignment Screenshots:

*Valid User Form Entry*:

![Valid](img/before.png)

*Passed Validation*:

![Passed](img/after.png)

*Associated Database Entry*:
![DB](img/db.png)
