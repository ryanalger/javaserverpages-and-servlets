# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Bitbucket Tutorials

#### Git commands w/ short descriptions:

1. git init - Create an empty git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and merge with another repository or local branch
7. git mv - Move or rename a file, directory or symlink

#### Assignment Screenshots:

*Screenshot of running http://localhost9999:*

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello:*

![JDK Installation Screenshot](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ryanalger@bitbucket.org/ryanalger/bitbucketstationlocations.git/ "Bitbucket Station Locations")

*Tutorial -  Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ryanalger/myteamquotes/ "My Team Quotes Tutorial")
