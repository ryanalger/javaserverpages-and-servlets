# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Assignment 4 Requirements:

*Goals:*

1. Clone starter files from Bitbucket
2. Work with MVC Pattern
3. Implement Basic Server-Side Data Validation

#### Assignment Screenshots:

*Failed Validation*:

![Failed](img/failed.png)

*Successful Validation*:

![Passed](img/passed.png)

