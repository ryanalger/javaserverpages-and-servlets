# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Project 1 Requirements:

*Goals:*

1. Clone lis_student_files repo from Bitbucket
2. Modify meta tags, navigation links and header tags
3. Adjust carousel images, slide content and timing
4. Use jQuery, HTML5 and regexp for data validation

#### Project Links:

[Portfolio Home Page](http://localhost:9999/lis4368/)

[Project Page](http://localhost:9999/lis4368/p1/index.jsp)

#### Project Screenshots:

*Failed Validation*:

![Failed](img/fail.png)

*Successful Validation*:

![Passed](img/pass.png)


