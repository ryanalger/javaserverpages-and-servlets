-- MySQL Script generated by MySQL Workbench
-- Tue Feb 21 20:10:25 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema rja16b
-- -----------------------------------------------------
-- Assignment 3 for Adv. Web App
DROP SCHEMA IF EXISTS `rja16b` ;

-- -----------------------------------------------------
-- Schema rja16b
--
-- Assignment 3 for Adv. Web App
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rja16b` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `rja16b` ;

-- -----------------------------------------------------
-- Table `rja16b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rja16b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `rja16b`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `rja16b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rja16b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `rja16b`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `rja16b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rja16b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `rja16b`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_customer_idx` (`cus_id` ASC),
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC),
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `rja16b`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `rja16b`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `rja16b`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `rja16b`;
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Bouncing Bunnies', '1922 Rhode Street', 'Tampa', 'FL', 33620, 8139214329, 'bouncing@bunnies.com', 'bouncingbunnies.com', 3123401, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', '235 N. Alt Drive', 'New York', 'NY', 83219, 6441239043, 'fuzzy@friends.com', 'fuzzyfriends.com', 2802190, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Playful Pups', '461 Sea Blvd', 'Miami', 'FL', 33073, 3052146689, 'playful@pups.com', 'playfulpups.com', 100100, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Cutie\'s Kittens', '301 Convication Way', 'Tallahassee', 'FL', 32304, 8507556819, 'cutie@kittens.com', 'cutieskittens.com', 1234567, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Flopping Fish', '1372 Oceanic Blvd', 'Rockledge', 'FL', 18720, 3219421054, 'flopping@fish.com', 'floppingfish.com', 923513, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Slithery Snakes', '901 Atlantic Blvd', 'Coral Springs', 'FL', 33071, 9543921083, 'slithery@snakes.com', 'slitherysnakes.com', 2093183, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Gordo Groundhogs', '8392 N.E. Apple Ct', 'Phoenix', 'AZ', 91230, 3220192343, 'gordo@groundhogs.com', 'gordogroundhogs.com', 9283102, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Crazy Chameleons', '566 S. North Road', 'Portland', 'OR', 42103, 9998887777, 'crazy@chameleons.com', 'crazychameleons.com', 621023, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Goofy Gophers', '488 E. West Drive', 'Stanton', 'NC', 23761, 5351239673, 'goofy@gophers.com', 'goofygophers.com', 9991234, NULL);
INSERT INTO `rja16b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Tenacious Tortoise', '101 Terrapin Way', 'Turtleville', 'MD', 74323, 9832651956, 'tenacious@tortoise.com', 'tenacioustortoise.com', 4567891, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `rja16b`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `rja16b`;
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'John', 'Doe', '928 Brook Rd', 'Scranton', 'NY', 74213, 9431674328, 'billybob@gmail.com', 902, 902, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jane', 'Doe', '928 Brook Rd', 'Scranton', 'NY', 74213, 9431234513, 'janedoe@hotmail.com', 55, 1032, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Buck', 'Strickland', '123 Freedom Drive', 'Arlen', 'TX', 98313, 4210452146, 'buck@stricklandpropane.com', 0, 9035, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jimmy', 'Johnson', '912 W. Pensacola Street', 'Tallahassee', 'FL', 32304, 8501928413, 'jj42@rocketmail.com', 0, 0, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Allison', 'Scott', '450 Ocean Drive', 'Fort Lauderdale', 'FL', 33071, 9542741845, 'ally.scott@gmail.com', 100, 100, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jenny', 'Jackson', '1200 High Road', 'Tallahassee', 'FL', 32304, 8501935831, 'jj41@rocketmail.com', 59, 59, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Andrew', 'Ronaldo', '323 E. Johnston Road', 'Phoenix', 'AZ', 36116, 4015624544, 'andyr@hotmail.com', 0, 309, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bill', 'Gates', '1835 73rd Ave', 'Medina', 'WA', 81723, 9913418345, 'mr.gates@microsoft.com', 0, 999999, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Christopher', 'Roberts', '822 42nd Street', 'New York', 'NY', 91423, 8081342341, 'croberts@outlook.com', 91, 8182, NULL);
INSERT INTO `rja16b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Billy', 'Bob', '505 6th Street', 'Tampa', 'FL', 33620, 8145261539, 'billy.bob33@gmail.com', 0, 1, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `rja16b`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `rja16b`;
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 3, 'dog', 'm', 200, 400, 2, 'brown', '2016-11-17', 'y', 'y', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 3, 'cat', 'f', 300, 450, 1, 'black', '2016-11-17', 'y', 'y', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 9, 'turtle', 'f', 5, 20, 3, 'green', '2017-02-12', 'n', 'n', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 7, 'hamster', 'm', 20, 40, 1, 'white', '2017-01-09', 'y', 'n', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 8, 'snake', 'm', 15, 300, 1, 'red', '2016-03-12', 'n', 'n', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, NULL, 'gopher', 'f', 7, 49, 2, 'brown', NULL, 'y', 'n', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, NULL, 'chipmunk', 'f', 5, 35, 1, 'brown', NULL, 'y', 'y', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 3, 'rooster', 'm', 8, 45, 5, 'white', '2017-02-09', 'y', 'n', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, NULL, 'cow', 'f', 900, 2500, 7, 'black', NULL, 'y', 'y', NULL);
INSERT INTO `rja16b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 10, 'monkey', 'm', 800, 2000, 3, 'brown', '2017-01-22', 'y', 'y', NULL);

COMMIT;

