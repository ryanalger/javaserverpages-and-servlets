# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Assignment 3 Requirements:

*Goals:*

1. Download and Install MySQL Workbench
2. Create Pet Store ERD
3. Forward Engineer to CCI Server

#### Assignment Links:

1. [a3.sql](https://bitbucket.org/ryanalger/lis4368/src/master/a3/docs/a3.sql)
2. [a3.wmb](https://bitbucket.org/ryanalger/lis4368/src/master/a3/docs/a3.mwb?at=master&fileviewer=file-view-default)

#### Assignment Screenshots:

*Screenshot of Pet Store ERD*:

![Pet Store ERD](img/pet_store_erd.png)


