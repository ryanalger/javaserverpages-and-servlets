# LIS4368 - Advanced Web Applications Development

## Ryan Alger

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Control with Git and Bitbucket
    - Java/JSP/Servlet Development Installation
    - Bitbucket Tutorials
	
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Install mySQL
	- Develop and Deploy a WebApp
	- Deploy a Servlet using @WebServlet
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Download and Install MySQL Workbench
	- Create Pet Store ERD
	- Forward Engineer to CCI Server

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Clone starter files from Bitbucket
	- Modify meta tags, navigation links and header tags
	- Adjust carousel images, slide content and timing
	- Use jQuery, HTML5 and regexp for data validation
	
5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Clone starter files from Bitbucket
	- Work with MVC Pattern
	- Implement Basic Server-Side Data Validation
	
6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Work with MVC Pattern
	- Prepared statements to prevent SQL injection
	- JSTL to prevent XSS
	
7. [p2 README.md](p2/README.md "My P2 README.md file")
	- Clone starter files from Bitbucket
	- Work with MVC Pattern
	- Prepared statements to prevent SQL injection
	- Use JSTL to prevent XSS
	- Enable full CRUD functionality