# LIS4368 - Advanced Web Applications Development

## Ryan Alger

### Project 2 Requirements:

1. Clone starter files from Bitbucket
2. Work with MVC Pattern
3. Prepared statements to prevent SQL injection
4. Use JSTL to prevent XSS
5. Enable full CRUD functionality

#### Project Screenshots:

*Valid Entry*:

![Valid](img/validform.png)

*Passes Validation*:

![Passed](img/passed.png)

*Valid Entry*:

![Valid](img/validform.png)

*Display Data*:

![Display Data](img/display.png)

*Modify Form*:

![Modify](img/modify.png)

*Modified Form*:

![Modified](img/modified.png)

*Delete Warning*:

![Delete](img/delete.png)

*Database Changes*

![DB Changes](img/db.png)